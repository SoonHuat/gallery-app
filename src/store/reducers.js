import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import app from '../modules/App/AppReducer'
import gallery from '../modules/Gallery/GalleryReducer'
import { reducer } from 'react-redux-sweetalert'
import { reducer as burgerMenu } from 'redux-burger-menu'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    app,
    gallery,
    sweetalert: reducer,
    form: formReducer,
    burgerMenu
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
