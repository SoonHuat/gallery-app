export const API_URL = process.env.NODE_ENV === 'development'
                      ? `http://localhost:3000/api`
                      : `http://YOUR_DOMAIN/api`
