import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import '../App.scss'
import { Switch, Route, Redirect } from 'react-router-dom'
import ReduxSweetAlert from 'react-redux-sweetalert'
import { action as toggleMenu } from 'redux-burger-menu'

import HomeContainer from '../../Home/containers/HomeContainer'

class App extends Component {
  componentDidMount () {
    this.props.history.listen( location =>  {
      this.props.dispatch(toggleMenu(false))
    })
  }
  render () {
    return (
      <div>
        <Switch>
          <Route exact path='/' component={HomeContainer} />
          <Route render={() => <Redirect to='/' />} />
        </Switch>
        <ReduxSweetAlert />
      </div>
    )
  }
}

App.propTypes = {
  children: PropTypes.node,
}

export default connect()(App)
