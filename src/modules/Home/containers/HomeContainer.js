import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import GalleryContainer from '../../Gallery/containers/GalleryContainer'

const HomeContainer = (props) => (
  <div>
    <GalleryContainer />
  </div>
)

export default HomeContainer
