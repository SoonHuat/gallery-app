import React, { Component } from 'react'
import InfiniteScroll from 'react-infinite-scroller'

class GalleryListComponent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      galleryList: []
    }
  }

  componentWillMount () {
    this.props.getGalleryList(1)
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      galleryList: nextProps.galleryList
    })
  }

  render () {
    return (
      <div>
        <h1>Gallery</h1>
        {
          this.state.galleryList.map((gallery) => (
            <img src={gallery.image_url} />
          ))
        }
      </div>
    )
  }
}
export default GalleryListComponent
