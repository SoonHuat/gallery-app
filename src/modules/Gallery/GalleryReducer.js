const initialState = {
  isRequesting: false,
  errorMessage: null,
  galleryList: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'REQUESTING_GALLERY':
      return Object.assign({}, state, {
        isRequesting: true
      })
    case 'SUCCESS_GET_GALLERY':
      return Object.assign({}, state, {
        isRequesting: false,
        galleryList: action.payload,
        errorMessage: null
      })
    case 'FAILED_GET_GALLERY':
      return Object.assign({}, state, {
        isRequesting: false,
        errorMessage: action.errorMessage
      })
    default:
      return state
  }
}
